import express from 'express';
import json from "body-parser";
import conexion from '../models/conexion.js';
export const router=express.Router();
import alumnosDb from "../models/alumnos.js";

router.get('/',(req,res)=>{
    res.render('index',{titulo:"Mi primer pagina ejs",nombre:"Jesus Alejandro Quezada Lara"})
});

// Ruta para la tabla (GET y POST)
router.get('/tabla', (req, res) => {
    const numero = req.query.numero ; 
    res.render('tabla', { numero: numero });
});

router.post('/tabla', (req, res) => {
    const numero = req.body.numero ;
    res.render('tabla', { numero: numero });
});

router.get('/cotizacion', (req, res) => {
    // Pasar valores predeterminados para evitar errores de undefined
    res.render('cotizacion', {
        pagoInicial: '---', // O puedes usar null y manejarlo en EJS
        total: '---',
        pagoMensual: '---'
    });
});

router.post('/cotizacion', (req, res) => {
    // Extraer los valores del formulario
    const precio = parseFloat(req.body.precio);
    const porcentajePagoInicial = parseFloat(req.body.porcentajePagoInicial);
    const plazos = parseInt(req.body.plazos, 10); // Asegúrate de que los plazos sean un número entero

    // Realizar los cálculos
    const pagoInicial = precio * (porcentajePagoInicial / 100);
    const total = precio - pagoInicial;
    const pagoMensual = total / plazos;

    // Renderizar la vista con los resultados
    res.render('cotizacion', {
        // Puedes pasar otros valores necesarios para tu vista aquí
        pagoInicial: pagoInicial, // Formatea a dos decimales
        total: total,
        pagoMensual: pagoMensual.toFixed(2)
    });
});


let rows;
    router.get('/alumnos',async(req,res)=>{
        rows = await alumnosDb.mostrarTodos();

        res.render('alumnos',{reg:rows});

    })
    let params;
    router.post('/alumnos', async(req,res)=>{
        try {

            params ={
            matricula:req.body.matricula,
            nombre:req.body.nombre,
            domicilio:req.body.domicilio,
            sexo : req.body.sexo,
            especialidad:req.body.especialidad
        }
        const registros = await alumnosDb.insertar(params);
        console.log("-------------- registros " + registros);

    } catch(error){
        console.error(error)
        res.status(400).send("sucedio un error: " + error);
    }
    rows = await alumnosDb.mostrarTodos();
        res.render('alumnos',{reg:rows});
    });

async function prueba(){
    try{
        const res= await conexion.execute("select * from alumnos");
        console.log("El resultado es ", res);
    }catch(error){
        console.log("Surgio un error". error);
    }
    finally{
        
    }
}

router.post("/buscar",async(req,res)=>{
    matricula=req.body.matricula;
    nrow=await alumnosDb.buscarMatricula(matricula);
    res.render("alumnos",{alu:nrow});
    
})
export default {router}